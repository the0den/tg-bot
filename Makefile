SHELL := /bin/bash
MAKEFILE_PATH := $(abspath $(dir $(abspath $(lastword $(MAKEFILE_LIST)))))
PATH := $(MAKEFILE_PATH):$(PATH)

# Basic go commands
GOCMD     = go
GOBUILD   = $(GOCMD) build
GOCLEAN   = $(GOCMD) clean
GOFMT     = $(GOCMD) fmt
GOGET     = $(GOCMD) get
GOINSTALL = $(GOCMD) install
GORUN     = $(GOCMD) run
GOTEST    = $(GOCMD) test
GOVET     = $(GOCMD) vet
GOMOD     = $(GOCMD) mod
GOTIDY    = $(GOMOD) tidy
GOUPDATE  = $(GOGET) -u
GOTYPE    = gotype

# Binary output name
BINARY = tg-bot
LINT = golangci-lint
SWAGGER = swagger
WGET = wget

#
BUILD_DIR    = $(CURDIR)
COVERAGE_DIR = $(BUILD_DIR)/coverage
SUBCOV_DIR   = $(COVERAGE_DIR)/packages

#
PKGS = $(shell go list ./...)

# Colors
GREEN_COLOR   = "\033[0;32m"
PURPLE_COLOR  = "\033[0;35m"
DEFAULT_COLOR = "\033[m"

.PHONY: all help clean test coverage vet lint type format build run install version

all: clean build format lint test

help:
	@echo 'Usage: make <TARGETS> ... <OPTIONS>'
	@echo ''
	@echo 'Available targets are:'
	@echo ''
	@echo '    build              Compile packages and dependencies.'
	@echo '    clean              Remove binary.'
	@echo '    coverage           Report code TESTS coverage.'
	@echo '    deps               Download required dependencies and remove unused'
	@echo '    update             Update dependencies'
	@echo '    format             Run gofmt on package sources.'
	@echo '    help               Show this help screen.'
	@echo '    install            Compile and install packages and dependencies.'
	@echo '    lint               Run linter on package sources.'
	@echo '    run                Compile and run Go program.'
	@echo '    test               Run unit TESTS.'
	@echo '    type               Run gotype'
	@echo '    version            Print Go version.'
	@echo '    vet                Run go tool vet on packages'
	@echo ''
	@echo 'Targets run by default are: clean format lint vet coverage test.'
	@echo ''

clean:
	@echo -e [$(GREEN_COLOR)clean$(DEFAULT_COLOR)]
	@$(GOCLEAN)
	@if [ -f $(BINARY) ] ; then rm $(BINARY) ; fi

test:
	@echo -e [$(GREEN_COLOR)test$(DEFAULT_COLOR)]
	@$(GOTEST) -race $(PKGS)

deps:
	@echo -e [$(GREEN_COLOR)resolve dependencies$(DEFAULT_COLOR)]
	@$(GOTIDY)

update:
	@echo -e [$(GREEN_COLOR)update dependencies$(DEFAULT_COLOR)]
	@$(GOUPDATE)

coverage:
	@echo -e [$(GREEN_COLOR)coverage$(DEFAULT_COLOR)]
	@# Create the coverage files directory
	@mkdir -p $(COVERAGE_DIR)/
	@mkdir -p $(SUBCOV_DIR)/
	@# Create a coverage file for each package
	@for package in $(PKGS); do $(GOTEST) -covermode=count -coverprofile $(SUBCOV_DIR)/`basename "$$package"`.cov "$$package"; done
	@# Merge the coverage profile files
	@echo 'mode: count' > $(COVERAGE_DIR)/coverage.cov ;
	@tail -q -n +2 $(SUBCOV_DIR)/*.cov >> $(COVERAGE_DIR)/coverage.cov ;
	@go tool cover -func=$(COVERAGE_DIR)/coverage.cov ;
	@# If needed, generate HTML report
	@if [ $(html) ]; then go tool cover -html=$(COVERAGE_DIR)/coverage.cov -o coverage.html ; fi
	@# Remove the coverage files directory
	@rm -rf $(COVERAGE_DIR);

vet:
	@echo -e [$(GREEN_COLOR)vet$(DEFAULT_COLOR)]
	@-$(GOVET) $(PKGS)

lint:
	@echo -e [$(GREEN_COLOR)lint$(DEFAULT_COLOR)]
	@if [ ! -x "$(shell command -v $(LINT))" ]; then \
		echo "---> installing golangci-lint"; \
		$(GOINSTALL) -v github.com/golangci/golangci-lint/cmd/golangci-lint; \
		echo "golangci-lint installed"; \
	fi
	@$(LINT) run --config=$(MAKEFILE_PATH)/.golangci.yml

type:
	@echo -e [$(GREEN_COLOR)type$(DEFAULT_COLOR)]
	@$(GOTYPE) -t -v $(PKGS)

format:
	@echo -e [$(GREEN_COLOR)format$(DEFAULT_COLOR)]
	@$(GOFMT) $(PKGS)

build:
	@echo -e [$(GREEN_COLOR)build$(DEFAULT_COLOR)]
	@$(GOBUILD) $(LDFLAGS) -o $(BINARY)

run: build
	@echo -e [$(GREEN_COLOR)run$(DEFAULT_COLOR)]
	@$(GORUN) -race main.go

install:
	@echo -e [$(GREEN_COLOR)install$(DEFAULT_COLOR)]
	@$(GOINSTALL) install $(LDFLAGS) ./...

version:
	@echo -e [$(GREEN_COLOR)version$(DEFAULT_COLOR)]
	@$(GOCMD) version
