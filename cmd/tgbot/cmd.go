package tgbot

import (
	"fmt"
	log "github.com/sirupsen/logrus"

	"github.com/go-telegram-bot-api/telegram-bot-api"
	"github.com/spf13/cobra"
)

var cfg config

var Cmd = &cobra.Command{
	Use:   "listener",
	Short: "simple telegram bot to handle commands",
	Run: func(cmd *cobra.Command, args []string) {
		if cfg.Validate() != nil {
			log.WithField("config", cfg).Fatal("misconfigured")
		}

		bot, err := tgbotapi.NewBotAPI(cfg.APIToken)
		if err != nil {
			log.WithError(err).Fatal("api init")
		}
		bot.Debug = true

		log.WithField("username", bot.Self.UserName).Info("Authorized on account")

		u := tgbotapi.NewUpdate(0)
		u.Timeout = 60

		updates, err := bot.GetUpdatesChan(u)
		if err != nil {
			log.WithError(err).Error("subscribing for updates")
		}

		for update := range updates {
			if update.Message == nil { // ignore any non-Message Updates
				continue
			}

			msg := tgbotapi.NewMessage(update.Message.Chat.ID, "")

			switch update.Message.Command() {
			case "get_id":
				ch, err := bot.GetChat(tgbotapi.ChatConfig{SuperGroupUsername: update.Message.Text})
				id := ch.ID
				if err != nil {
					log.WithError(err).Error("getting chat")
					id = update.Message.Chat.ID
				}

				msg.Text = fmt.Sprintf("Your id is: %d", id)

			default:
				msg.Text = fmt.Sprintf("Your id is: %d", update.Message.Chat.ID)
				msg.ReplyToMessageID = update.Message.MessageID
			}

			if _, err := bot.Send(msg); err != nil {
				log.WithError(err).Fatal("sending id")
			}
		}
	},
}

func init() {
	Cmd.Flags().AddFlagSet(cfg.Flags())
}
